// check and initialize the IDs
// the number of edges
if (isset($_POST['check'])) {
	$numOfEdges = $_POST['check'];
}
// for output to be ints (flag)
if (isset($_POST['intCount'])) {
	$intCount = $_POST['intCount'];
}
// the input file name (and the respective path)
if (isset($_POST['value'])) {
	$value = $_POST['value'];
}

print "Value of $numOfEdges: " . $numOfEdges;
echo "Value of $intCount: " . $intCount;
echo "Value of $value: " . $value;

// preconcatenated command line arguments
$edgecmd = 'python/sanitize.py ' . $value;
$maincmd = '../wrappers/subgraph_counts.py ' . ' ' . $value . $numOfEdges;

// check to see if the extension is an edges file
if (strcmp($value, "edges") == 0){
	ob_start();
	
	// concatenate the integer flag
	if (strcmp($check, "-i") == 0){
		$maincmd = $maincmd . ' -i';
	}
	
// a check to see if the extension is a text file
} else if (strcmp($value, "txt") == 0) {
	ob_start();
	// concatenate the integer flag
	if (strcmp($check, "-i") == 0){
		$maincmd = $maincmd . ' -i';
	}

	// test
	echo $maincmd;
}
