var check = null;	// acting flag for 3, 4, or 5 vertex subgraph
var intCount = "-i";	// flag for the script
var edges = false;	// flag for edges file input
var txt = false;	// flag for text file
var value = null;	// holder variable for the input file name
var ext = null;		// extension "holder"

// whether the count is true or not
function intcount(element){
	if (element.checked) !element.checked;
	else if (!element.checked) element.checked;
	console.log(Boolean(element.checked));	// test
}

// check if argument is three vertex subgraph
function checkThree(){
	check = 3;
	console.log(check);	// test
}

// check if argument is four vertex subgraph
function checkFour(){
	check = 4;
	console.log(check);	// test
}

// check if argument is five vertex subgraph
function checkFive(){
	check = 5;
	console.log(check);	//test
}

// disable the text file input and render the edges file
function setEdges(){
	// disable text file
	document.getElementById("txt").disabled = true;
	edges = true;	// edges file has been submitted
	console.log(edges);	//test
}

// disable the edges file input and render the text file
function setTxt(){
	// disable edges file
	document.getElementById("edges").disabled = true;
	txt = true;	// edges file has been submitted
	console.log(txt);	//test
}

function appendGraph(data) {
	var $one, $two, oneLabels, circle_ids, line_ids
	$("#left-graph").empty()
	$("#right-graph").empty()

	data.forEach(function (item) {
		$one = $("<div>", { class: "one", "id": item["type"] })
		$two = $("<div>", { class: "two" })

		circle_ids = item["circle_ids"]
		line_ids = item["line_ids"]

		circle_ids.forEach(function (id) {
			var $circle = $("<div>", { class: "three circle", "id": id })
			$two.append($circle)
		})

		line_ids.forEach(function (id) {
			var $line = $("<div>", { class: "three line", "id": id })
			$two.append($line)
		})

		$one.append($two)
		$one.append("<div class='bottomtext two'>" + $one.attr('id') + "</div>")
		$("#left-graph").append($one)

		var $clone = $one.clone();
		$("#right-graph").append($clone)
	})
}

// when the submit button is hit
function submit() {
	var data = []

	if (edges === true){
		document.getElementById("txt").disabled = false;
	
		// check if the file has a valid edges extension
		value = document.getElementById("edges").value;
		console.log(value);	// test
		ext = /(\.edges)$/i;
		if (!ext.exec(value)) alert("Invalid file extension!");
		
		$.ajax({
			url: '../index.php',
			type:'POST',
			dataType: "json",
			data: {
				check: check,
				intCount: intCount,
				value: value
			}
		}).done(function(data){
			alert(JSON.stringify(data));
		});

	} else if (txt == true) {
		document.getElementById("edges").disabled = false;

		// check if the file has a valid txt extension
		value = document.getElementById("txt").value;
		console.log(value);	// test
		ext = /(\.txt)$/i;
		if (!ext.exec(value)) alert("Invalid file extension!");
		
		$.ajax({
			url: '../index.php',
			type:'POST',
			dataType: "json",
			data: {
				check: check,
				intCount: intCount,
				value: value
			}
		}).done(function(data){
			alert(JSON.stringify(data));
		});

	} else alert("Please enter a text file!");

	if (check === 'null') {
		alert("Please select either a 3, 4, or 5 vertex subgraph!");
	}
	else if (check === 3) {
		data =
			[
				{
					"type": "Ind Set",
					"circle_ids": [
						"T-L-3",
						"T-R-3",
						"B-M-3"
					],
					"line_ids": [

					]
				},
				{
					"type": "Only Edge",
					"circle_ids": [
						"T-L-3",
						"T-R-3",
						"B-M-3"
					],
					"line_ids": [
						"TL3-TR3"
					]
				},
				{
					"type": "Wedge",
					"circle_ids": [
						"T-L-3",
						"T-R-3",
						"B-M-3"
					],
					"line_ids": [
						"TL3-TR3",
						"TL3-BM3"
					]
				},
				{
					"type": "Triangle",
					"circle_ids": [
						"T-L-3",
						"T-R-3",
						"B-M-3"
					],
					"line_ids": [
						"TL3-TR3",
						"TL3-BM3",
						"TR3-BM3"
					]
				}
			]

		appendGraph(data)
	} else if (check === 4) {
		data =
			[
				{
					"type": "Ind Set",
					"circle_ids": [
						"T-L-4",
						"T-R-4",
						"B-L-4",
						"B-R-4"
					],
					"line_ids": [

					]
				},
				{
					"type": "Only Edge",
					"circle_ids": [
						"T-L-4",
						"T-R-4",
						"B-L-4",
						"B-R-4"
					],
					"line_ids": [
						"TL4-TR4"
					]
				},
				{
					"type": "Matching",
					"circle_ids": [
						"T-L-4",
						"T-R-4",
						"B-L-4",
						"B-R-4"
					],
					"line_ids": [
						"TL4-TR4",
						"BL4-BR4"
					]
				},
				{
					"type": "Only Wedge",
					"circle_ids": [
						"T-L-4",
						"T-R-4",
						"B-L-4",
						"B-R-4"
					],
					"line_ids": [
						"TL4-TR4",
						"TR4-BL4"
					]
				},
				{
					"type": "Only Triangle",
					"circle_ids": [
						"T-L-4",
						"T-R-4",
						"B-L-4",
						"B-R-4"
					],
					"line_ids": [
						"TL4-TR4",
						"TR4-BL4",
						"TL4-BL4"
					]
				},
				{
					"type": "3-Star",
					"circle_ids": [
						"T-L-4",
						"T-R-4",
						"B-L-4",
						"B-R-4"
					],
					"line_ids": [
						"TL4-BL4",
						"BL4-BR4",
						"TR4-BL4"
					]
				},
				{
					"type": "3-Path",
					"circle_ids": [
						"T-L-4",
						"T-R-4",
						"B-L-4",
						"B-R-4"
					],
					"line_ids": [
						"TL4-BL4",
						"TL4-TR4",
						"BL4-BR4"
					]
				},
				{
					"type": "Tailed-Triangle",
					"circle_ids": [
						"T-L-4",
						"T-R-4",
						"B-L-4",
						"B-R-4"
					],
					"line_ids": [
						"TL4-BL4",
						"TL4-TR4",
						"BL4-BR4",
						"TR4-BL4"
					]
				},
				{
					"type": "4-Cycle",
					"circle_ids": [
						"T-L-4",
						"T-R-4",
						"B-L-4",
						"B-R-4"
					],
					"line_ids": [
						"TL4-BL4",
						"TR4-BR4",
						"TL4-TR4",
						"BL4-BR4"
					]
				},
				{
					"type": "Diamond",
					"circle_ids": [
						"T-L-4",
						"T-R-4",
						"B-L-4",
						"B-R-4"
					],
					"line_ids": [
						"TL4-BL4",
						"TR4-BR4",
						"TL4-TR4",
						"BL4-BR4",
						"TR4-BL4"
					]
				},
				{
					"type": "4-Clique",
					"circle_ids": [
						"T-L-4",
						"T-R-4",
						"B-L-4",
						"B-R-4"
					],
					"line_ids": [
						"TL4-BL4",
						"TR4-BR4",
						"TL4-TR4",
						"BL4-BR4",
						"TL4-BR4",
						"TR4-BL4"
					]
				}
			]
		appendGraph(data)
	} else if(check === 5){
		data =
			[
				{
					"type": "Ind Set",
					"circle_ids": [
						"T-LM",
						"T-MR",
						"C-L",
						"C-R",
						"B-M"
					],
					"line_ids": [
						
					]
				},
				{
					"type": "Only Edge",
					"circle_ids": [
						"T-LM",
						"T-MR",
						"C-L",
						"C-R",
						"B-M"
					],
					"line_ids": [
						"TL-TR"
					]
				},
				{
					"type": "Matching",
					"circle_ids": [
						"T-LM",
						"T-MR",
						"C-L",
						"C-R",
						"B-M"
					],
					"line_ids": [
						"TL-TR",
						"CL-CR"
					]
				},
				{
					"type": "Only Wedge",
					"circle_ids": [
						"T-LM",
						"T-MR",
						"C-L",
						"C-R",
						"B-M"
					],
					"line_ids": [
						"TL-TR",
						"TLM-CL"
					]
				},
				{
					"type": "Only Triangle",
					"circle_ids": [
						"T-LM",
						"T-MR",
						"C-L",
						"C-R",
						"B-M"
					],
					"line_ids": [
						"TL-TR",
						"TLM-CL",
						"TMR-CL"
					]
				},
				{
					"type": "Only 3-Star",
					"circle_ids": [
						"T-LM",
						"T-MR",
						"C-L",
						"C-R",
						"B-M"
					],
					"line_ids": [
						"TL-TR",
						"TLM-CL",
						"TLM-CR"
					]
				},
				{
					"type": "Only 3-Path",
					"circle_ids": [
						"T-LM",
						"T-MR",
						"C-L",
						"C-R",
						"B-M"
					],
					"line_ids": [
						"TL-TR",
						"CL-CR",
						"TLM-CL"
					]
				},
				{
					"type": "Only Tailed-Triangle",
					"circle_ids": [
						"T-LM",
						"T-MR",
						"C-L",
						"C-R",
						"B-M"
					],
					"line_ids": [
						"TL-TR",
						"CL-CR",
						"TLM-CL",
						"TMR-CL"
					]
				},
				{
					"type": "Only 4-Cycle",
					"circle_ids": [
						"T-LM",
						"T-MR",
						"C-L",
						"C-R",
						"B-M"
					],
					"line_ids": [
						"TL-TR",
						"CL-CR",
						"TMR-CR",
						"TLM-CL"
					]
				},
				{
					"type": "Only Diamond",
					"circle_ids": [
						"T-LM",
						"T-MR",
						"C-L",
						"C-R",
						"B-M"
					],
					"line_ids": [
						"TL-TR",
						"CL-CR",
						"TMR-CR",
						"TLM-CL",
						"TLM-CR"
					]
				},
				{
					"type": "Only 4-Clique",
					"circle_ids": [
						"T-LM",
						"T-MR",
						"C-L",
						"C-R",
						"B-M"
					],
					"line_ids": [
						"TL-TR",
						"CL-CR",
						"TMR-CR",
						"TLM-CL",
						"TLM-CR",
						"TMR-CL"
					]
				},
				{
					"type": "Wedge+edge",
					"circle_ids": [
						"T-L",
						"T-R",
						"C-M",
						"B-L",
						"B-R"
					],
					"line_ids": [
						"TL-CM",
						"CM-BL",
						"TR-CM",
						"CM-BR"
					]
				},
				{
					"type": "Triangle+edge",
					"circle_ids": [
						"T-M",
						"C-L",
						"C-M",
						"C-R",
						"B-L"
					],
					"line_ids": [
						"TM-CL",
						"TM-CM",
						"TM-CR",
						"CL-BL"
					]
				},
				{
					"type": "4-star",
					"circle_ids": [
						"T-M",
						"C-LM",
						"C-MR",
						"B-LM",
						"B-MR"
					],
					"line_ids": [
						"TM-CLM",
						"TM-CMR",
						"CLM-BLM",
						"CMR-BMR"
					]
				},
				{
					"type": "Prong",
					"circle_ids": [
						"T-LM",
						"T-MR",
						"C-M",
						"B-LM",
						"B-MR"
					],
					"line_ids": [
						"TLM-CM",
						"CM-BLM",
						"TMR-CM",
						"CM-BMR",
						"TL-TR"
					]
				},
				{
					"type": "4-path",
					"circle_ids": [
						"T-M",
						"C-LM",
						"C-MR",
						"CB-M",
						"B-MR"
					],
					/*line over circles*/
					"line_ids": [
						"TM-CLM",
						"TM-CMR",
						"CL-CR1",
						"CLM-BMR"
					]
				},
				{
					"type": "Forktailed-triangle",
					"circle_ids": [
						"T-M",
						"C-LM",
						"C-MR",
						"B-LM",
						"B-MR"
					],
					"line_ids": [
						"TM-CLM",
						"TM-CMR",
						"CLM-BLM",
						"CMR-BMR",
						"CL-CR1"
					]
				},
				{
					"type": "Longtailed-triangle",
					"circle_ids": [
						"T-LM",
						"T-MR",
						"C-LM",
						"C-MR",
						"B-M"
					],
					"line_ids": [
						"TL-TR",
						"CL-CR1",
						"TLM-CLM",
						"TMR-CMR",
						"CLM-BM"
					]
				},
				{
					"type": "Doubletailed-triangle",
					"circle_ids": [
						"T-LM",
						"T-MR",
						"C-L",
						"C-R",
						"B-M"
					],
					"line_ids": [
						"TL-TR",
						"CL-BM",
						"CR-BM",
						"TMR-CR",
						"TLM-CL"
					]
				},
				{
					"type": "Tailed-4-cycle",
					"circle_ids": [
						"T-LM",
						"T-MR",
						"C-M",
						"B-LM",
						"B-MR"
					],
					"line_ids": [
						"TLM-CM",
						"CM-BLM",
						"TMR-CM",
						"CM-BMR",
						"TL-TR",
						"BL-BR"
					]
				},
				{
					"type": "5-cycle",
					"circle_ids": [
						"T-LM",
						"T-MR",
						"C-LM",
						"C-MR",
						"B-M"
					],
					"line_ids": [
						"TL-TR",
						"CL-CR1",
						"TLM-CLM",
						"TMR-CMR",
						"CLM-BM",
						"TLM-CMR"						
					]
				},
				{
					"type": "Hourglass",
					"circle_ids": [
						"T-LM",
						"T-MR",
						"C-LM",
						"C-MR",
						"B-M"
					],
					"line_ids": [
						"TL-TR",
						"CL-CR1",
						"TLM-CLM",
						"TMR-CMR",
						"CLM-BM",
						"TMR-CLM"
					]
				},
				{
					"type": "Cobra",
					"circle_ids": [
						"T-LM",
						"T-MR",
						"C-LM",
						"C-MR",
						"B-M"
					],
					"line_ids": [
						"TL-TR",
						"CL-CR1",
						"TLM-CLM",
						"TMR-CMR",
						"CLM-BM",
						"CMR-BM"
					]
				},
				{
					"type": "Stingray",
					"circle_ids": [
						"T-MMR",
						"C-L",
						"C-LMM",
						"C-R",
						"B-MMR"
					],
					"line_ids": [
						"TMMR-CR",
						"TMMR-CL",
						"TMMR-CLMM",
						"CR-BMMR",
						"CL-BMMR",
						"CLMM-BMMR"
					]
				},
				{
					"type": "Hatted-4-cycle",
					"circle_ids": [
						"T-MMR",
						"C-L",
						"C-LMM",
						"C-R",
						"B-MMR"
					],
					"line_ids": [
						"TMMR-BMMR",
						"TMMR-CR",
						"TMMR-CL",
						"TMMR-CLMM",
						"CR-BMMR",
						"CL-BMMR",
						"CLMM-BMMR",
					]
				},
				{
					"type": "3-wedge-collision",
					"circle_ids": [
						"T-LM",
						"T-MR",
						"C-LM",
						"C-MR",
						"B-M"
					],
					"line_ids": [
						"TL-TR",
						"CL-CR1",
						"CLM-BM",
						"TLM-CMR",
						"TLM-CLM",
						"TMR-CMR",
						"TMR-CLM"
					]
				},
				{
					"type": "3-triangle-collision",
					"circle_ids": [
						"T-LM",
						"T-MR",
						"C-LM",
						"C-MR",
						"B-M"
					],
					"line_ids": [
						"TL-TR",
						"CL-CR1",
						"CLM-BM",
						"CMR-BM",
						"TLM-CLM",
						"TMR-CMR",
						"TMR-CLM"
					]
				},
				{
					"type": "Tailed-4-clique",
					"circle_ids": [
						"T-LM",
						"T-MR",
						"C-L",
						"C-R",
						"B-M"
					],
					"line_ids": [
						"CL-BM",
						"CR-BM",
						"TL-TR",
						"TMR-CR",
						"TLM-CL",
						"TLM-CR",
						"TMR-CL"
					]
				},
				{
					"type": "Triangle-strip",
					"circle_ids": [
						"T-L",
						"T-R",
						"C-M",
						"B-L",
						"B-R"
					],
					"line_ids": [
						"TL-TR1",
						"CM-BL",
						"TL-CM",
						"TR-CM",
						"CM-BR",
						"BL-BR1",
						"TL-BL",
						"TR-BR"
					]
				},
				{
					"type": "Diamond-wedge-column",
					"circle_ids": [
						"T-LM",
						"T-MR",
						"C-LM",
						"C-MR",
						"B-M"
					],
					"line_ids": [
						"TL-TR",
						"CL-CR1",
						"CLM-BM",
						"CMR-BM",
						"TMR-CMR",
						"TLM-CLM",
						"TMR-CLM",
						"TLM-CMR"
					]
				},
				{
					"type": "4-wheel",
					"circle_ids": [
						"T-LM",
						"T-MR",
						"C-L",
						"C-R",
						"B-M"
					],
					"line_ids": [
						"CL-CR",
						"CL-BM",
						"CR-BM",
						"TMR-CR",
						"TLM-CL",
						"TLM-CR",
						"TMR-CL"
					]
				},
				{
					"type": "Hatted-4-clique",
					"circle_ids": [
						"T-LM",
						"T-MR",
						"C-L",
						"C-R",
						"B-M"
					],
					"line_ids": [
						"TL-TR",
						"CL-CR",
						"CL-BM",
						"CR-BM",
						"TMR-CR",
						"TLM-CL",
						"TLM-CR",
						"TMR-CL",
						"TLM-BM",
						"TMR-BM"
					]
				}
			]
		appendGraph(data)
	}
}
