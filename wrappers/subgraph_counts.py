# This is the main wrapper function for getting subgraph counts. It simply calls
# the appropriate executables, which output the number of non-induced patterns. This code
# applies appropriate linear transformations to get all induced counts. It outputs to data to
# an html file where data is visualized
#
# The function should be called as follows:
#
# python3 <PATH TO INPUT> <DESIRED PATTERN SIZE> <OPTIONAL FLAGS>
#
# DESIRED PATTERN SIZE: Either 3, 4, or 5
# OPTIONAL FLAGS:
#         -i: output counts as integers. Useful for small graphs, or for debugging
#
#
# Output format:
#
# Each line contains the name of a pattern, the number of non-induced occurrences, the number
# of induced occurrences, and the ratio between these.
#
# Akul Goyal: Created a visualization of the data set after algorithum has run on it
#

import os
import numpy as np
import sys
import math
import time
from shape import draw
from ascii_graph import Pyasciigraph
from bs4 import BeautifulSoup
import shutil
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import mpld3 as mpld3


N = 20
graph_name = (' Ind set', ' Only edge',  'Ind set','Only edge','Matching','Only wedge','Only triangle','Ind set ','Only edge ','Matching ','Only wedge ','Only triangle ',
    'Only 3-star ','Only 3-path ','Only Tailed tri ','Only 4-cycle ','Only Diamond ','Only 4-clique ','Wedge+edge ','Triangle+edge ' )
num_stat= []
induced_count=[]
noninduced_count=[]
INduced_count=[]
heatmaps=[]
induced_count_heat=[]
noninduced_count_heat=[]

matrices = dict() # conversion matrices: converting induced to non-induced

matrices[3] = np.matrix('1 1 1 1 ;'
                        '0 1 2 3 ;'
                        '0 0 1 3 ;'
                        '0 0 0 1 ')

matrices[4] = np.matrix('1 1 1 1 1 1 1 1 1 1 1 ;'
                        '0 1 2 2 3 3 3 4 4 5 6 ;'
                        '0 0 1 0 0 0 1 1 2 2 3 ;'
                        '0 0 0 1 3 3 2 5 4 8 12 ;'
                        '0 0 0 0 1 0 0 1 0 2 4 ;'
                        '0 0 0 0 0 1 0 1 0 2 4 ;'
                        '0 0 0 0 0 0 1 2 4 6 12 ;'
                        '0 0 0 0 0 0 0 1 0 4 12 ;'
                        '0 0 0 0 0 0 0 0 1 1 3 ;'
                        '0 0 0 0 0 0 0 0 0 1 6 ;'
                        '0 0 0 0 0 0 0 0 0 0 1')

matrices[5] = np.matrix('1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 ;'
                        '0 1 2 2 3 3 3 4 4 5 6 3 4 4 4 4 5 5 5 5 5 6 6 6 6 6 7 7 7 7 8 8 9 10 ;'
                        '0 0 1 0 0 0 1 1 2 2 3 2 3 0 2 3 2 4 3 4 5 5 5 4 6 6 6 6 7 8 10 9 12 15 ;'
                        '0 0 0 1 3 3 2 5 4 8 12 1 3 6 4 3 8 6 7 6 5 10 10 11 9 9 15 15 14 13 18 19 24 30 ;'
                        '0 0 0 0 1 0 0 1 0 2 4 0 1 0 0 0 1 1 1 0 0 2 2 2 1 0 3 4 3 2 4 5 7 10 ;'
                        '0 0 0 0 0 1 0 1 0 2 4 0 0 4 1 0 4 1 2 1 0 4 3 5 2 2 8 7 6 4 8 10 14 20 ;'
                        '0 0 0 0 0 0 1 2 4 6 12 0 0 0 2 2 4 4 5 6 5 8 10 10 10 12 18 18 17 18 28 28 42 60 ;'
                        '0 0 0 0 0 0 0 1 0 4 12 0 0 0 0 0 2 1 2 0 0 4 5 6 2 0 12 15 10 6 16 22 36 60 ;'
                        '0 0 0 0 0 0 0 0 1 1 3 0 0 0 0 0 0 0 0 1 0 0 1 1 1 3 3 3 2 3 5 5 9 15 ;'
                        '0 0 0 0 0 0 0 0 0 1 6 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 3 6 2 1 4 8 15 30 ;'
                        '0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 2 5 ;'
                        '0 0 0 0 0 0 0 0 0 0 0 1 3 0 1 2 1 4 2 3 5 6 5 3 7 6 6 6 9 11 16 13 21 30 ;'
                        '0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 1 0 0 0 2 1 0 1 0 0 1 2 2 4 3 6 10 ;'
                        '0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 1 0 0 0 0 1 0 1 0 0 2 1 1 0 1 2 3 5 ;'
                        '0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 2 1 2 2 0 4 4 5 4 6 12 9 10 10 20 20 36 60 ;'
                        '0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 2 1 2 5 4 4 2 7 6 6 6 10 14 24 18 36 60 ;'
                        '0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 2 0 2 0 0 6 3 3 0 4 8 15 30 ;'
                        '0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 4 2 0 2 0 0 3 6 6 16 12 30 60 ;'
                        '0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 2 2 1 0 6 6 5 4 12 14 30 60 ;'
                        '0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 1 1 2 6 6 3 4 8 16 12 30 60 ;'
                        '0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 0 0 0 1 2 4 2 6 12 ;'
                        '0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 1 0 2 2 6 15 ;'
                        '0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 3 2 2 8 8 24 60 ;'
                        '0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 6 3 2 0 4 10 24 60 ;'
                        '0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 2 4 12 6 24 60 ;'
                        '0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 1 2 1 4 10 ;'
                        '0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 1 3 10 ;'
                        '0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 2 6 20 ;'
                        '0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 4 4 18 60 ;'
                        '0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 4 1 9 30 ;'
                        '0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 3 15 ;'
                        '0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 6 30 ;'
                        '0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 10 ;'
                        '0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1'
)

inputFile=None

if len(sys.argv) < 3:
    print( 'Format: python3 subgraph_counts.py <PATH FOR INPUT> <DESIRED PATTERN SIZE> <OPTIONS>\n DESIRED PATTERN SIZE: 3, 4, or 5\n OPTIONS:\n \t'+
                    '-i: for integer outputs (useful in looking at small graphs), fileName to port data to, must be in the same location as workDir, does not have to exist' )
    sys.exit()

#checks to see if an inputFile is being passed in input
if len(sys.argv)==4:
        print(sys.argv[3])
        if '-i' not in sys.argv:
            inputFile=sys.argv[3]


integral = False
if '-i' in sys.argv:
    integral = True

pattern = int(sys.argv[2])  # size of desired patterns

if pattern == 3:   # calling appropriate executable, output is in out.txt
    os.system('../exe/count_three '+sys.argv[1])
elif pattern == 4:
    os.system('../exe/count_four '+sys.argv[1])
elif pattern == 5:
    os.system('../exe/count_five '+sys.argv[1])
else:
    print('Incorrect format: Desired pattern size must be 3, 4, or 5')
    sys.exit()

f_out = open('out.txt','r')

noninduced = dict() # storing all noninduced in a dictionary of lists

noninduced[3] = list()  # list for each possible size
noninduced[4] = list()
noninduced[5] = list()

num_lines = 1
for line in f_out.readlines():
    current = float(line.strip())
    if num_lines == 1:
        n = current
    if num_lines == 2:
        m = current
    if num_lines >= 3 and num_lines <= 6:
        noninduced[3].append(current)
    if num_lines >= 7 and num_lines <= 17:
        noninduced[4].append(current)
    if num_lines >= 18 and num_lines <= 51:
        noninduced[5].append(current)
    num_lines = num_lines + 1

f_out.close()

# converto numpy arrays
for i in range(3,6):
    if len(noninduced[i]) > 0:
        noninduced[i] = np.array(noninduced[i])

induced = dict()
induced[3] = list()
induced[4] = list()
induced[5] = list()


for i in range(3,6):
    if len(noninduced[i]) > 0:
        induced[i] = np.linalg.solve(matrices[i],noninduced[i])  #inverting matrices[i] to convert non-induced to induced noninduced

print('==============')
print('Basic size')
print('==============')

print('Vertices\t',math.floor(n))
print('Edges\t\t',math.floor(m))



names = dict()

names[3] = [' Ind set', ' Only edge', ' Wedge', ' Triangle']
names[4] = ['Ind set ','Only edge ','Matching ','Only wedge ','Only triangle ',
           '3-star ','3-path ','Tailed triangle ','4-cycle ','Diamond ',
           '4-clique ']
names[5] = ['Ind set ','Only edge ','Matching ','Only wedge ','Only triangle ',
           'Only 3-star ','Only 3-path ','Only Tailed tri ','Only 4-cycle ',
           'Only Diamond ','Only 4-clique ','Wedge+edge ','Triangle+edge ',
           '4-star ','Prong ','4-path ','Forktailed-tri ','Lontailed-tri ',
           'Doubletailed-tri ','Tailed-4-cycle ','5-cycle ','Hourglass','Cobra',
           'Stingray','Hatted-4-cycle','3-wedge-col','3-tri-collision',
           'Tailed-4-clique','Triangle-strip','Diamond-wed-col','4-wheel',
           'Hatted-4-clique','Almost-5-clique','5-clique']



typeFile = 0

if inputFile is None:
    curDir = os.getcwd()
    curDirc = curDir + '/html_output/subgraphs-c.html'
    curDirH = curDir + '/html_output/html_code/'
else:
    curDir = os.getcwd()
    curDirc = curDir + '/html_output/' + inputFile
    curDirH = curDir + '/html_output/html_code/'

if len(induced[4]) != 0:
     typeFile=1
if len(induced[5]) != 0:
     typeFile=2
if typeFile == 0:
    with open(curDirH+'subgraph3.html') as f:
            with open(curDirc, "w") as f1:
                for line in f:
                    f1.write(line)
    f.close()
    f1.close()
if typeFile == 1:
     with open(curDirH+'subgraph4.html') as f:
            with open(curDirc, "w") as f1:
                for line in f:
                        f1.write(line)
     f.close()
     f1.close()
if typeFile == 2:
     with open(curDirH+'subgraph5.html') as f:
            with open(curDirc, "w") as f1:
                for line in f:
                    f1.write(line)
     f.close()
     f1.close()

for i in range(3,6):
    if len(induced[i]) == 0:
        break
    print('==============')
    print(str(i)+' vertex patterns')
    print('==============')
    print("\n")

    if i==3:

        with open(curDirc) as inf:
            txt = inf.read()
            soup = BeautifulSoup(txt, 'html.parser')

        for j in range(0,len(induced[i])):
            if noninduced[i][j] == 0:
                if integral:
                    title= str(i)+'.'+str(j+1)
                    print (title)
                    x=(soup.find(id=title))

                    new_tag = soup.new_tag("div", **{'class':'two bottomtext2'})
                    new_tag.string = str(math.floor(noninduced[i][j]))+ "  "+str(math.floor(induced[i][j]))+'  -'
                    if x is None:
                        continue

                    noninduced_count.append(noninduced[i][j])
                    induced_count.append(induced[i][j])
                    INduced_count.append(induced[i][j]/noninduced[i][j])

                    x.insert_after(new_tag)
                else:
                    title= str(i)+'.'+str(j+1)
                    print(title)
                    x=(soup.find(id=title))

                    new_tag = soup.new_tag("div", **{'class': 'two bottomtext2'})
                    new_tag.string = '%.2e'%noninduced[i][j] + "  "+ '%.2e'%induced[i][j] + '  -'
                    if x is None:
                        continue
                    noninduced_count.append(noninduced[i][j])
                    induced_count.append(induced[i][j])
                    INduced_count.append(induced[i][j]/noninduced[i][j])

                    x.insert_after(new_tag)


            else:
                if integral:
                    title = str(i)+'.'+str(j+1)
                    print(title)
                    x = (soup.find(id=title))

                   # print(x)
                    new_tag = soup.new_tag("div", **{'class': 'two bottomtext2'})
                    new_tag.string = str(math.floor(noninduced[i][j]))+"  "+str(math.floor(induced[i][j]))+"  "+'%.2f'%(induced[i][j]/noninduced[i][j])
                   # print(new_tag)
                    if x is None:
                        continue
                    noninduced_count.append(noninduced[i][j])
                    induced_count.append(induced[i][j])
                    INduced_count.append(induced[i][j]/noninduced[i][j])


                    x.insert_after(new_tag)

                else:
                    title= str(i)+'.'+str(j+1)
                    print(title)
                    x=(soup.find(id=title))

                 #   print(x)
                    new_tag = soup.new_tag("div", **{'class': 'two bottomtext2'})
                    new_tag.string = '%.2e'%noninduced[i][j]+" "+'%.2e'%induced[i][j]+"  "+'%.2f'%(induced[i][j]/noninduced[i][j])
                    if x is None:
                        continue
                    noninduced_count.append(noninduced[i][j])
                    induced_count.append(induced[i][j])
                    INduced_count.append(induced[i][j]/noninduced[i][j])
                    x.insert_after(new_tag)
        inf.close()


        html = soup.prettify("utf-8")
        with open(curDirc, "wb") as file:
            file.write(html)

    if i==4:

        with open(curDirc) as inf:
            txt = inf.read()
            soup = BeautifulSoup(txt, 'html.parser')
        for j in range(0,len(induced[i])):
            if noninduced[i][j] == 0:
                if integral:
                    title= str(i)+'.'+str(j+1)
                    print (title)
                    x=(soup.find(id=title))

                    new_tag = soup.new_tag("div", **{'class':'two bottomtext2'})
                    new_tag.string = str(math.floor(noninduced[i][j]))+"  "+str(math.floor(induced[i][j]))+"  "+'-'
                    if x is None:
                        continue
                    noninduced_count.append(noninduced[i][j])
                    induced_count.append(induced[i][j])
                    INduced_count.append(induced[i][j]/noninduced[i][j])
                    x.insert_after(new_tag)
                else:
                    title= str(i)+'.'+str(j+1)
                    print(title)
                    x=(soup.find(id=title))

                    new_tag = soup.new_tag("div", **{'class': 'two bottomtext2'})
                    new_tag.string = '%.2e'%noninduced[i][j] + "  "+'%.2e'%induced[i][j] +"  "+ '-'
                    if x is None:
                        continue
                    noninduced_count.append(noninduced[i][j])
                    induced_count.append(induced[i][j])
                    INduced_count.append(induced[i][j]/noninduced[i][j])
                    x.insert_after(new_tag)

            else:
                if integral:
                    title= str(i)+'.'+str(j+1)
                    print(title)
                    x=(soup.find(id=title))

                    new_tag = soup.new_tag("div", **{'class': 'two bottomtext2'})
                    new_tag.string = str(math.floor(noninduced[i][j]))+"  "+str(math.floor(induced[i][j]))+"  "+'%.2f'%(induced[i][j]/noninduced[i][j])
                    if x is None:
                        continue
                    noninduced_count.append(noninduced[i][j])
                    induced_count.append(induced[i][j])
                    INduced_count.append(induced[i][j]/noninduced[i][j])
                    x.insert_after(new_tag)

                else:
                    title= str(i)+'.'+str(j+1)
                    print(title)
                    x=(soup.find(id=title))

                    new_tag = soup.new_tag("div", **{'class': 'two bottomtext2'})
                    new_tag.string = '%.2e'%noninduced[i][j]+"  "+'%.2e'%induced[i][j]+"  "+'%.2f'%(induced[i][j]/noninduced[i][j])
                    if x is None:
                        continue
                    noninduced_count.append(noninduced[i][j])
                    induced_count.append(induced[i][j])
                    INduced_count.append(induced[i][j]/noninduced[i][j])
                    x.insert_after(new_tag)
        inf.close()

        html = soup.prettify("utf-8")
        with open(curDirc, "wb") as file:
            file.write(html)

    if i==5:

        with open(curDirc) as inf:
            txt = inf.read()
            soup = BeautifulSoup(txt, 'html.parser')
        for j in range(0,len(induced[i])):
            if noninduced[i][j] == 0:
                if integral:
                    title= str(i)+'.'+str(j+1)
                    print (title)
                    x=(soup.find(id=title))

                    new_tag = soup.new_tag("div", **{'class':'two bottomtext2'})
                    new_tag.string = str(math.floor(noninduced[i][j]))+"  "+str(math.floor(induced[i][j]))+"  "+'-'
                    induced_count_heat.append(induced[i][j])
                    noninduced_count_heat.append(noninduced[i][j])
                    if x is None:
                        continue
                    noninduced_count.append(noninduced[i][j])
                    induced_count.append(induced[i][j])
                    INduced_count.append(induced[i][j]/noninduced[i][j])
                    x.insert_after(new_tag)
                    new_tag = soup.new_tag("div", **{'class': 'two bottomtext2'})
                    new_tag.string = names[i][j]
                    x.insert_after(new_tag)
                else:
                    title= str(i)+'.'+str(j+1)
                    print(title)
                    x=(soup.find(id=title))

                    new_tag = soup.new_tag("div", **{'class': 'two bottomtext2'})
                    new_tag.string = '%.2e'%noninduced[i][j] + "  "+'%.2e'%induced[i][j] +"  "+ '-'
                    induced_count_heat.append(induced[i][j])
                    noninduced_count_heat.append(noninduced[i][j])
                    if x is None:
                        continue
                    noninduced_count.append(noninduced[i][j])
                    induced_count.append(induced[i][j])
                    INduced_count.append(induced[i][j]/noninduced[i][j])
                    x.insert_after(new_tag)
                    new_tag = soup.new_tag("div", **{'class': 'two bottomtext2'})
                    new_tag.string = names[i][j]
                    x.insert_after(new_tag)

            else:
                if integral:
                    title= str(i)+'.'+str(j+1)
                    print(title)
                    x=(soup.find(id=title))

                    new_tag = soup.new_tag("div", **{'class': 'two bottomtext2'})
                    new_tag.string = str(math.floor(noninduced[i][j]))+"  "+str(math.floor(induced[i][j]))+"  "+'%.2f'%(induced[i][j]/noninduced[i][j])
                    induced_count_heat.append(induced[i][j])
                    noninduced_count_heat.append(noninduced[i][j])
                    if x is None:
                        continue
                    noninduced_count.append(noninduced[i][j])
                    induced_count.append(induced[i][j])
                    INduced_count.append(induced[i][j]/noninduced[i][j])
                    x.insert_after(new_tag)
                    new_tag = soup.new_tag("div", **{'class': 'two bottomtext2'})
                    new_tag.string = names[i][j]
                    x.insert_after(new_tag)

                else:
                    title= str(i)+'.'+str(j+1)
                    print(title)
                    x=(soup.find(id=title))

                    new_tag = soup.new_tag("div", **{'class': 'two bottomtext2'})
                    new_tag.string = '%.2e'%noninduced[i][j]+"  "+'%.2e'%induced[i][j]+"  "+'%.2f'%(induced[i][j]/noninduced[i][j])
                    induced_count_heat.append(induced[i][j])
                    noninduced_count_heat.append(noninduced[i][j])
                    if x is None:
                        continue
                    noninduced_count.append(noninduced[i][j])
                    induced_count.append(induced[i][j])
                    INduced_count.append(induced[i][j]/noninduced[i][j])
                    x.insert_after(new_tag)
                    new_tag = soup.new_tag("div", **{'class': 'two bottomtext2'})
                    new_tag.string = names[i][j]
                    x.insert_after(new_tag)
        inf.close()

        objects = ('Wedge', 'Triangle', '3-star','3-path','Tailed triangle','4-cycle','Diamond',
            '4-clique', '4-star','Prong','4-path','Forktailed-tri','Lontailed-tri',
            'Doubletailed-tri','Tailed-4-cycle','5-cycle','Hourglass','Cobra',
            'Stingray','Hatted-4-cycle','3-wedge-col','3-tri-collision',
            'Tailed-4-clique','Triangle-strip','Diamond-wed-col','4-wheel',
            'Hatted-4-clique','Almost-5-clique','5-clique')

        # y_pos = np.arange(len(objects))
        # performance = INduced_count
        # fig=plt.bar(y_pos, performance, align='center')
        # plt.xticks(y_pos, objects, rotation='vertical')
        # plt.title('Induced/NonInduced Count')
        # mpl.rcParams.update({'font.size': 5})
        # plt.savefig('divINduced.png', bbox_inches='tight')
        # x=(soup.find(id='i.3'))
        # new_tag = soup.new_tag('img', src=os.getcwd()+'/divINduced.png')
        # x.insert_after(new_tag)
        # #plt.show()
        #
        #
        # y_pos = np.arange(len(objects))
        # performance = induced_count
        # fig=plt.bar(y_pos, performance, align='center', log=True)
        # plt.xticks(y_pos, objects, rotation='vertical')
        # plt.title('Induced Count')
        # mpl.rcParams.update({'font.size': 5})
        # #plt.show()
        # # figs,ax=plt.subplots(1)
        # # figs.autofmt_xdate()
        # plt.savefig('induced.png', bbox_inches='tight')
        # x=(soup.find(id='i.1'))
        # new_tag = soup.new_tag('img', src=os.getcwd()+'/induced.png')
        # x.insert_after(new_tag)
        #
        # y_pos = np.arange(len(objects))
        # performance = noninduced_count
        # fig=plt.bar(y_pos, performance, align='center', log=True)
        # plt.xticks(y_pos, objects, rotation='vertical')
        # plt.title('Noniduced Count')
        # mpl.rcParams.update({'font.size': 5})
        # plt.savefig('noninduced.png', bbox_inches='tight')
        # x=(soup.find(id='i.2'))
        # new_tag = soup.new_tag('img', src=os.getcwd()+'/noninduced.png')
        # x.insert_after(new_tag)
        a = np.zeros(shape=(34,34))
        for i in range(34):
            for j in range(i,34):
                print(matrices[5].item(i,j))
                if (matrices[5].item(i,j)*induced_count_heat[j])/noninduced_count_heat[i] == 0:
                    a[i][j]=0.0
                else:
                    a[i][j]=(((matrices[5].item(i,j)*induced_count_heat[j])/noninduced_count_heat[i])*10)

                #print((matrices[5].item(i,j)*induced_count_heat[j])/noninduced_count_heat[i])
        plt.imshow(matrices[5],interpolation='none')
        plt.show()










        html = soup.prettify("utf-8")
        with open(curDirc, "wb") as file:
            file.write(html)
